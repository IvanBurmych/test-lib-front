import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Answer } from 'src/app/models/answer';
import { Question } from 'src/app/models/question';
import { Test } from 'src/app/models/test';
import { TestCollectionService } from 'src/app/services/test-collection.service';
import { Router } from '@angular/router';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { ResultService } from 'src/app/services/result.service';
import { SendResult } from 'src/app/models/sendResult';

export interface TestData {
  answerId: number;
  isTrue: boolean;
}

@Component({
  selector: 'concretetest',
  templateUrl: './concrete-test.component.html',
  styleUrls: ['./concrete-test.component.css']
})
export class ConcreteTestComponent implements OnInit {

  id: number = 0;

  test: Test;
  question: Question;
  data: TestData[];
  iterator: number = 0;
  answer: Answer;
  countTrues: number = 0;

  constructor(private route: ActivatedRoute, private ts: TestCollectionService, private rs: ResultService, private router: Router) { 
    this.id = route.snapshot.params['id'];
    this.answer = {id: 0, text:' ', isTrue: false};
    this.question = {id: 0, text:' ', answers:[this.answer]};
    this.test = {id: 0, name:'',description:'',questions:[this.question]};
    this.data = [];
  }

  ngOnInit(): void {
    this.ts.getMyTestById(this.id)
      .subscribe(res => {
          this.test = res;
          this.question = res.questions[this.iterator];
      })
  }

  onClick(answerId: number): void{

    let isTrue = this.trueOrFalseAnswer(answerId);

    if(isTrue){
      this.countTrues++;
    }

    this.data.push({answerId: answerId, isTrue: isTrue});
    this.iterator++;
    if(this.iterator >= this.test.questions.length){
      this.rs.addMyResult(
        {testName: this.test.name, correctAnswersCount: this.countTrues, wrongAnswersCount: (this.iterator - this.countTrues)}
      ).subscribe();

      this.router.navigate([`/result/${this.countTrues}/${this.iterator}`]);
    }
    
    this.question = this.test.questions[this.iterator];
  }

  trueOrFalseAnswer(id: number): boolean{
    let ans = false;
    
    for(let i = 0; i < this.question.answers.length; i++){
      if(this.question.answers[i].id === id){
        ans = this.question.answers[i].isTrue;
        break;
      }
    }
    return ans;
  }
}
