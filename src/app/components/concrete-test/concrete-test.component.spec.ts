import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcreteTestComponent } from './concrete-test.component';

describe('ConcreteTestComponent', () => {
  let component: ConcreteTestComponent;
  let fixture: ComponentFixture<ConcreteTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConcreteTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcreteTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
