import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Test } from 'src/app/models/test';
import { TestCollectionService } from 'src/app/services/test-collection.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  tests: Test[] = [];
  columns = ['id','name','description','start']

  constructor(private ts: TestCollectionService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.ts.getMyTests()
      .subscribe(res => {
        this.tests = res
      })
  }

  openDialog(id: number, description: string): void {
    const dialogRef = this.dialog.open(Dialog, {
      width: '460px',
      height: '340px',
      data: { id: id, description: description}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
  
}

export interface DialogData {
  id: number;
  description: string;
}

@Component({
  selector: 'dialog',
  templateUrl: './dialog.html'
})
export class Dialog implements OnInit {

  isChecked = false;

  constructor(
    public dialogRef: MatDialogRef<Dialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private router: Router
  ) {}

  ngOnInit(){

  }

  onClick(): void{
    if(this.isChecked){
      this.dialogRef.close();
      this.router.navigate([`/concretetest/${this.data.id}`])
    }
  }

}