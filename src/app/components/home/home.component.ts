import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}


/*
import { Component, OnInit } from '@angular/core';
import { Test } from 'src/app/models/test';
import { TestCollectionService } from 'src/app/services/test-collection.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tests: Test[] = [];
  columns = ['id','name','description']

  constructor(private ts: TestCollectionService) { }

  ngOnInit(): void {
    this.ts.getMyTests()
      .subscribe(res => {
        this.tests = res
      })
  }

}
*/