import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  countTrues: number = 0;
  count: number = 0;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.countTrues = route.snapshot.params['countTrues'];
    this.count = route.snapshot.params['count'];
   }

  ngOnInit(): void {
  }

  onClick(): void {
    this.router.navigate([`/test`]);
  }
}
