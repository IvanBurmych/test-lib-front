import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { API_URL } from './app-injection-tokens';
import { JwtModule } from '@auth0/angular-jwt';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { Dialog, TestComponent } from './components/test/test.component';
import { ACCESS_TOKEN_KEY } from './services/auth.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConcreteTestComponent } from './components/concrete-test/concrete-test.component';
import { MatRadioModule } from '@angular/material/radio';
import { ResultComponent } from './components/result/result.component';

export function tokenGetter(){
  return localStorage.getItem(ACCESS_TOKEN_KEY);
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TestComponent,
    Dialog,
    ConcreteTestComponent,
    ResultComponent
  ],
  entryComponents: [Dialog],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,

    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatDialogModule,
    MatCheckboxModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: () => {return localStorage.getItem("access_token")},
        allowedDomains: environment.tokenWhitelistedDomains
      }
    })
  ],
  providers: [{
    provide: API_URL,
    useValue: environment.api
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
