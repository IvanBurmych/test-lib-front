
export interface SendResult{
    testName: string;
    correctAnswersCount: number;
    wrongAnswersCount: number;
}