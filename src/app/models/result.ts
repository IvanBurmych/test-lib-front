
export interface Result{
    id: number;
    userId: string;
    testName: string;
    correctAnswersCount: number;
    wrongAnswersCount: number;
}