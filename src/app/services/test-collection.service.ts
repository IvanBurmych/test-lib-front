import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../app-injection-tokens';
import { Test } from '../models/test';

@Injectable({
  providedIn: 'root'
})
export class TestCollectionService {

  private testApiUrl = `${this.apiUrl}api/test/`

  constructor(private http: HttpClient, @Inject(API_URL) private apiUrl: string) { }

  getMyTests(): Observable<Test[]>{
    return this.http.get<Test[]>(`${this.testApiUrl}my`)
  }
  getMyTestById(id: number): Observable<Test>{
    return this.http.get<Test>(`${this.testApiUrl}myById/${id}`)
  }
}
