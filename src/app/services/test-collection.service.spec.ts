import { TestBed } from '@angular/core/testing';

import { TestCollectionService } from './test-collection.service';

describe('TestCollectionService', () => {
  let service: TestCollectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestCollectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
