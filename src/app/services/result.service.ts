import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../app-injection-tokens';
import { Result } from '../models/result';
import { SendResult } from '../models/sendResult';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  private testApiUrl = `${this.apiUrl}api/result/`

  constructor(private http: HttpClient, @Inject(API_URL) private apiUrl: string) { }

  getMyResults(): Observable<Result[]>{
    return this.http.get<Result[]>(`${this.testApiUrl}my`);
  }
  addMyResult(result: SendResult){
    return this.http.post(`${this.testApiUrl}add`, result);
  }
}
