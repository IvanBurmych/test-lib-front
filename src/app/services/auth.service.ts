import { HttpClient } from '@angular/common/http';
import { Token } from '../models/token';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, tap } from 'rxjs';
import { API_URL } from '../app-injection-tokens';

export const ACCESS_TOKEN_KEY = 'access_token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private apiUrl: string,
    private jwtHelper: JwtHelperService,
    private router: Router
  ) { }

  login(Email: string, password: string): Observable<Token> {
    return this.http.post<Token>(`${this.apiUrl}api/login/SignIn`, {
      Email, password
    }).pipe(
      tap(token => {
        localStorage.setItem(ACCESS_TOKEN_KEY, token.access_token)
      })
    )
  }
  isAuthenticated(): boolean {
    var token = localStorage.getItem(ACCESS_TOKEN_KEY);
    var res = token != null && !this.jwtHelper.isTokenExpired(token);
    return res;
  }

  logup(Email: string, password: string, userName: string) {
    return this.http.post(`${this.apiUrl}api/login/SignUp`, {
      Email, password, userName
    })
  }

  logout(): void{
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    this.router.navigate(['']);
  }
}
