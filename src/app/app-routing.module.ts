import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConcreteTestComponent } from './components/concrete-test/concrete-test.component';
import { HomeComponent } from './components/home/home.component';
import { ResultComponent } from './components/result/result.component';
import { TestComponent } from './components/test/test.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'test', component: TestComponent},
  {path: 'concretetest/:id', component: ConcreteTestComponent},
  {path: 'result/:countTrues/:count', component: ResultComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
