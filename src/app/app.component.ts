import { Component, OnInit } from '@angular/core';
import { Result } from './models/result';
import { AuthService } from './services/auth.service';
import { ResultService } from './services/result.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  results: Result[] = [];
  hideLoggedInForm: boolean = false;

  constructor(private as: AuthService, private rs: ResultService){

  }

  public get isLoggedIn(): boolean{
    return this.as.isAuthenticated();
  }

  isItemsShowed: boolean = false;

  login(email: string, password: string){
    this.as.login(email, password)
      .subscribe(res => {

      }, error => {
        alert('wrong email or password');
      })
  }
  logout(){
    this.as.logout()
  }

  logup(email: string, password: string, userName: string){
    this.as.logup(email, password, userName)
      .subscribe(res => {

      })
    this.hideLoggedInForm = false
  }

  deleteMyRes(){
    this.results = [];
    this.isItemsShowed = false;
  }

  hideLoggedIn(){
    this.hideLoggedInForm = true;
  }

  showLoggedIn(){
    this.hideLoggedInForm = false;
  }

  getMyRes(){
    this.rs.getMyResults()
    .subscribe(res => {
      this.results = res;
    })
    this.isItemsShowed = true;
  }
}
